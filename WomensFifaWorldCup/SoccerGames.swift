//
//  SoccerGames.swift
//  WomensFifaWorldCup WatchKit Extension
//
//  Created by MacStudent on 2019-03-11.
//  Copyright © 2019 CaroliniTavares. All rights reserved.
//

import WatchKit

var gameList:[SoccerGames] = []
//var subGameList:[SoccerGames] = []
var sub:[String] = []
var printList:[SoccerGames] = []

class SoccerGames: NSObject {
    // MARK: class properties
    var id:Int?
    var team1:String?
    var team2:String?
    var location:String?
    var startTime:String?
    var date:String?
    var flag1:String?
    var flag2:String?
    var group:String?
    var subs:Bool?
    
    // MARK: contructor
    convenience override init() {
        //let d = Date()
        self.init(id:0, team1:"", team2:"", location:"", startTime: "", date:"", flag1: "", flag2: "", group: "", subs: false)
    }
    
    init(id:Int, team1:String, team2:String, location:String, startTime:String, date:String, flag1:String, flag2:String, group:String, subs:Bool) {
        self.id = id
        self.team1 = team1
        self.team2 = team2
        self.location = location
        self.startTime = startTime
        self.date = date
        self.flag1 = flag1
        self.flag2 = flag2
        self.group = group
        self.subs = subs
    }
}

func createGameObjects() {
    //let d = Date()
    //let g1 = BasketballGame(team1: "Trail Blazers", team2: "Raptors", location: "Toronto", startTime: d)
    let g1 = SoccerGames(id: 1, team1: "France", team2: "Korea", location: "Paris", startTime: "15:00", date:"06/07/2019", flag1: "France", flag2: "South_Korea", group: "Group A", subs: false)
    
    let g2 = SoccerGames(id: 2, team1: "Germany", team2: "China", location: "Rennes", startTime: "09:00", date:"06/08/2019", flag1: "Germany", flag2: "China", group: "Group B", subs: false)
    
    let g3 = SoccerGames(id: 3, team1: "Norway", team2: "Nigeria", location: "Reims", startTime: "15:00", date:"06/08/2019", flag1: "Norway", flag2: "Nigeria", group: "Group A", subs: false)
    
    let g4 = SoccerGames(id: 4, team1: "Brazil", team2: "Jamaica", location: "Grenoble", startTime: "09:30", date:"06/09/2019", flag1: "Brazil", flag2: "Jamaica", group: "Group C", subs: false)
    
    let g5 = SoccerGames(id: 5, team1: "Argentina", team2: "Japan", location: "Paris", startTime: "12:00", date:"06/10/2019", flag1: "Argentina", flag2: "Japan", group: "Group D", subs: false)
    
    gameList.append(g1)
    gameList.append(g2)
    gameList.append(g3)
    gameList.append(g4)
    gameList.append(g5)
}
