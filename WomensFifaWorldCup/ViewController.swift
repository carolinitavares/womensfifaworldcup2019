//
//  ViewController.swift
//  WomensFifaWorldCup
//
//  Created by MacStudent on 2019-03-11.
//  Copyright © 2019 CaroliniTavares. All rights reserved.
//

import UIKit
import WatchConnectivity

class ViewController: UIViewController, WCSessionDelegate {
    //MARK: Data Source
    var gameList:[SoccerGames] = []
    
    @IBAction func button(_ sender: Any) {
        // check if the watch is paired / accessible
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            let message = ["game1": "1",
                           "game2": "0",
                           "game3": "0",
                           "game4": "0",
                           "game5": "0"]
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    /*func createGameObjects() {
        //let d = Date()
        //let g1 = BasketballGame(team1: "Trail Blazers", team2: "Raptors", location: "Toronto", startTime: d)
        let g1 = SoccerGames(id: 1, team1: "France", team2: "Korea", location: "Paris", startTime: "15:00", date:"06/07/2019", flag1: "France", flag2: "South_Korea", group: "Group A")
        
        let g2 = SoccerGames(id: 2, team1: "Germany", team2: "China", location: "Rennes", startTime: "09:00", date:"06/08/2019", flag1: "Germany", flag2: "China", group: "Group B")
        
        let g3 = SoccerGames(id: 3, team1: "Norway", team2: "Nigeria", location: "Reims", startTime: "15:00", date:"06/08/2019", flag1: "Norway", flag2: "Nigeria", group: "Group A")
        
        let g4 = SoccerGames(id: 4, team1: "Brazil", team2: "Jamaica", location: "Grenoble", startTime: "09:30", date:"06/09/2019", flag1: "Brazil", flag2: "Jamaica", group: "Group C")
        
        let g5 = SoccerGames(id: 5, team1: "Argentina", team2: "Japan", location: "Paris", startTime: "12:00", date:"06/10/2019", flag1: "Argentina", flag2: "Japan", group: "Group D")
        
        gameList.append(g1)
        gameList.append(g2)
        gameList.append(g3)
        gameList.append(g4)
        gameList.append(g5)
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("checking to see if wc session is supported")
        
        //self.createGameObjects()
        
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        // MARK: Populate your tableview with data
        // ------------------
        // 0. tell IOS how many rows your table should have
        /*self.gameTable.setNumberOfRows(self.gameList.count, withRowType:"myRow")
        
        
        print("number of rows: \(self.gameList.count)")
        // 1. loop through your array
        // 2. take each item in the array and put it in a table row
        for (i, game) in self.gameList.enumerated() {
            
            let row = self.gameTable.rowController(at: i) as! SoccerRowController
            
            row.team1Name.setText(game.team1!)
            row.team2Name.setText(game.team2!)
            row.locationLabel.setText(game.location)
            row.timeLabel.setText(game.startTime)
            row.dateLabel.setText(game.date)
            row.team1Image.setImage(UIImage(named:game.flag1!))
            row.team2Image.setImage(UIImage(named:game.flag2!))
            row.groupLabel.setText(game.group)
        }*/

    }


}

