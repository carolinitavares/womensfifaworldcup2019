//
//  TableViewController.swift
//  WomensFifaWorldCup
//
//  Created by MacStudent on 2019-03-11.
//  Copyright © 2019 CaroliniTavares. All rights reserved.
//

import UIKit
import WatchConnectivity

class TableViewController: UITableViewController, WCSessionDelegate {
    
    
    @IBOutlet var table: UITableView!
    
    //var gameList:[SoccerGames] = []
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    @IBAction func switchClick(_ sender: Any) {
        if let indexPath = getIndexPath(of: sender){
            if(gameList[indexPath.row].subs!){
                gameList[indexPath.row].subs! = false
            }else{
                gameList[indexPath.row].subs! = true
            }
            //gameList[indexPath.row].subs! = !gameList[indexPath.row].subs!
        }
        
        if (WCSession.default.isReachable) {
            // construct the message you want to send
            // the message is in dictionary
            
            let message = ["game1": "\(gameList[0].subs! ? "1" : "0")",
                           "game2": "\(gameList[1].subs! ? "1" : "0")",
                           "game3": "\(gameList[2].subs! ? "1" : "0")",
                           "game4": "\(gameList[3].subs! ? "1" : "0")",
                           "game5": "\(gameList[4].subs! ? "1" : "0")"]
            // send the message to the watch
            WCSession.default.sendMessage(message, replyHandler: nil)
        }
    }
    
    private func getIndexPath(of element:Any) -> IndexPath?
    {
        if let view =  element as? UIView
        {
            // Converting to table view coordinate system
            let pos = view.convert(CGPoint.zero, to: self.tableView)
            // Getting the index path according to the converted position
            return tableView.indexPathForRow(at: pos) as? IndexPath
        }
        return nil
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }

    /*
    func createGameObjects() {
        //let d = Date()
        //let g1 = BasketballGame(team1: "Trail Blazers", team2: "Raptors", location: "Toronto", startTime: d)
        let g1 = SoccerGames(id: 1, team1: "France", team2: "Korea", location: "Paris", startTime: "15:00", date:"06/07/2019", flag1: "France", flag2: "South_Korea", group: "Group A", subs: false)
        
        let g2 = SoccerGames(id: 2, team1: "Germany", team2: "China", location: "Rennes", startTime: "09:00", date:"06/08/2019", flag1: "Germany", flag2: "China", group: "Group B", subs: false)
        
        let g3 = SoccerGames(id: 3, team1: "Norway", team2: "Nigeria", location: "Reims", startTime: "15:00", date:"06/08/2019", flag1: "Norway", flag2: "Nigeria", group: "Group A", subs: false)
        
        let g4 = SoccerGames(id: 4, team1: "Brazil", team2: "Jamaica", location: "Grenoble", startTime: "09:30", date:"06/09/2019", flag1: "Brazil", flag2: "Jamaica", group: "Group C", subs: false)
        
        let g5 = SoccerGames(id: 5, team1: "Argentina", team2: "Japan", location: "Paris", startTime: "12:00", date:"06/10/2019", flag1: "Argentina", flag2: "Japan", group: "Group D", subs: false)
        
        gameList.append(g1)
        gameList.append(g2)
        gameList.append(g3)
        gameList.append(g4)
        gameList.append(g5)
    }*/
    
    override func viewDidLoad() {
        super.viewDidLoad()

        createGameObjects()
        
        if (WCSession.isSupported()) {
            print("Yes it is!")
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        self.tableView.rowHeight = 144.0
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return gameList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myRow", for: indexPath) as! SoccerTableViewCell

        
        cell.team1Label.text = gameList[indexPath.row].team1
        cell.team2Label.text = gameList[indexPath.row].team2
        cell.locationLabel.text = gameList[indexPath.row].location
        cell.timeLabel.text = gameList[indexPath.row].startTime
        cell.dateLabel.text = gameList[indexPath.row].date
        cell.team1image.image = UIImage(named:gameList[indexPath.row].flag1!)
        cell.team2Image.image = UIImage(named:gameList[indexPath.row].flag2!)
        cell.groupLabel.text = gameList[indexPath.row].group
        cell.subscribedSwitch.isOn = gameList[indexPath.row].subs!

        return cell
    }
 
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat
    {
        return 144.0;//Choose your custom row height
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
