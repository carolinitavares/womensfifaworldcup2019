//
//  SubInterfaceController.swift
//  WomensFifaWorldCup WatchKit Extension
//
//  Created by MacStudent on 2019-03-11.
//  Copyright © 2019 CaroliniTavares. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity


class SubInterfaceController: WKInterfaceController, WCSessionDelegate {

    /*var subGameList:[SoccerGames] = []
    var sub:[String] = []
    var printList:[SoccerGames] = []*/
    
    @IBOutlet weak var subGameTable: WKInterfaceTable!
    @IBOutlet weak var titleLabel: WKInterfaceLabel!
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        // Play a "click" sound when you get the message
        WKInterfaceDevice().play(.click)
        
        // output a debug message to the terminal
        print("Got a message!")
        sub.removeAll()
        printList.removeAll()
        printTable()
        
        /*for i in 1...5{
            sub.append(message["game\(i)"] as! String)
            print("game\(i) = \(sub[i])")
        }*/
        //self.sub = message as! [String]
        let game1 = message["game1"] as! String
        let game2 = message["game2"] as! String
        let game3 = message["game3"] as! String
        let game4 = message["game4"] as! String
        let game5 = message["game5"] as! String
        
        sub.append(game1)
        sub.append(game2)
        sub.append(game3)
        sub.append(game4)
        sub.append(game5)
        
        print ("-> number subscriptions = \(sub.count)")
        print ("-> game 1 = \(sub[0])")
        print ("-> game 2 = \(sub[1])")
        print ("-> game 3 = \(sub[2])")
        print ("-> game 4 = \(sub[3])")
        print ("-> game 5 = \(sub[3])")
        

        printTable()
                
        //subGameList = message["game1"] as! [String]
        // update the message with a label
        //messageLabel.setText("\(messageRe)")
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
    }
    
    /*func createGameObjects() {
        //let d = Date()
        //let g1 = BasketballGame(team1: "Trail Blazers", team2: "Raptors", location: "Toronto", startTime: d)
        let g1 = SoccerGames(id: 1, team1: "France", team2: "Korea", location: "Paris", startTime: "15:00", date:"06/07/2019", flag1: "France", flag2: "South_Korea", group: "Group A", subs: false)
        
        let g2 = SoccerGames(id: 2, team1: "Germany", team2: "China", location: "Rennes", startTime: "09:00", date:"06/08/2019", flag1: "Germany", flag2: "China", group: "Group B", subs: false)
        
        let g3 = SoccerGames(id: 3, team1: "Norway", team2: "Nigeria", location: "Reims", startTime: "15:00", date:"06/08/2019", flag1: "Norway", flag2: "Nigeria", group: "Group A", subs: false)
        
        let g4 = SoccerGames(id: 4, team1: "Brazil", team2: "Jamaica", location: "Grenoble", startTime: "09:30", date:"06/09/2019", flag1: "Brazil", flag2: "Jamaica", group: "Group C", subs: false)
        
        let g5 = SoccerGames(id: 5, team1: "Argentina", team2: "Japan", location: "Paris", startTime: "12:00", date:"06/10/2019", flag1: "Argentina", flag2: "Japan", group: "Group D", subs: false)
        
        subGameList.append(g1)
        subGameList.append(g2)
        subGameList.append(g3)
        subGameList.append(g4)
        subGameList.append(g5)
    }*/

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        createGameObjects()
        
        if WCSession.isSupported() {
            let session = WCSession.default
            session.delegate = self
            session.activate()
        }
        
        printTable()
        
    }
    
    func printTable(){
        // MARK: Populate your tableview with data
        // ------------------
        // 0. tell IOS how many rows your table should have
        
        for (i, help) in sub.enumerated() {
            if (help == "1"){
                //print(subGameList[i].id)
                printList.append(gameList[i])
            }
        }
        if (printList.count > 0 ){
            self.subGameTable.setNumberOfRows(printList.count, withRowType:"subMyRow")
            
            print ("number sub = \(sub.count)")
            
            //print("number of rows: \(subGameList.count)")
            // 1. loop through your array
            // 2. take each item in the array and put it in a table row
       
            titleLabel.setText("Subscribed")
            subGameTable.setHidden(false)
            for (i, game) in printList.enumerated() {
                
                //if (sub[i] == "1") {
                    let row = self.subGameTable.rowController(at: i) as! SubSoccerRowController
                    
                    row.team1Name.setText(game.team1!)
                    row.team2Name.setText(game.team2!)
                    row.locationLabel.setText(game.location)
                    row.timeLabel.setText(game.startTime)
                    row.dateLabel.setText(game.date)
                    row.team1Image.setImage(UIImage(named:game.flag1!))
                    row.team2Image.setImage(UIImage(named:game.flag2!))
                    row.groupLabel.setText(game.group)
                
                //}
            }
        }else{
            self.subGameTable.setNumberOfRows(0, withRowType:"subMyRow")
            printList.removeAll()
            titleLabel.setText("No subscribes")
            subGameTable.setHidden(true)
        }
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    /*override func contextForSegue(withIdentifier segueIdentifier: String) -> Any? {
        if (segueIdentifier == "subIdentifier"){
            print("sub -> back")
        }
        return 0
    }*/
}
