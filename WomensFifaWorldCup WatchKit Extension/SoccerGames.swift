//
//  SoccerGames.swift
//  WomensFifaWorldCup WatchKit Extension
//
//  Created by MacStudent on 2019-03-11.
//  Copyright © 2019 CaroliniTavares. All rights reserved.
//

import WatchKit

class SoccerGames: NSObject {
    // MARK: class properties
    var id:Int?
    var team1:String?
    var team2:String?
    var location:String?
    var startTime:String?
    var date:String?
    var flag1:String?
    var flag2:String?
    var group:String?
    var subs:Bool?
    
    // MARK: contructor
    convenience override init() {
        //let d = Date()
        self.init(id:0, team1:"", team2:"", location:"", startTime: "", date:"", flag1: "", flag2: "", group: "", subs: false)
    }
    
    init(id:Int, team1:String, team2:String, location:String, startTime:String, date:String, flag1:String, flag2:String, group:String, subs:Bool) {
        self.id = id
        self.team1 = team1
        self.team2 = team2
        self.location = location
        self.startTime = startTime
        self.date = date
        self.flag1 = flag1
        self.flag2 = flag2
        self.group = group
        self.subs = subs
    }
}
