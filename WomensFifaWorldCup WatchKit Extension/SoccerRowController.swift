//
//  SoccerRowController.swift
//  WomensFifaWorldCup WatchKit Extension
//
//  Created by MacStudent on 2019-03-11.
//  Copyright © 2019 CaroliniTavares. All rights reserved.
//

import WatchKit

class SoccerRowController: NSObject {

    @IBOutlet weak var team1Image: WKInterfaceImage!
    @IBOutlet weak var team1Name: WKInterfaceLabel!
    @IBOutlet weak var team2Image: WKInterfaceImage!
    @IBOutlet weak var team2Name: WKInterfaceLabel!
    @IBOutlet weak var timeLabel: WKInterfaceLabel!
    @IBOutlet weak var dateLabel: WKInterfaceLabel!
    @IBOutlet weak var locationLabel: WKInterfaceLabel!
    @IBOutlet weak var groupLabel: WKInterfaceLabel!
}
